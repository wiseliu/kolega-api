<?php

namespace App;

use Illuminate\Database\Eloquent\Relations\Pivot;

class ProjectFavourite extends Pivot
{
    protected $fillable = ['id', 'poject_id', 'user_id',];
    
    public function user()
    {
        return $this->belongsTo('App\User');
    }
    
    public function project()
    {
        return $this->belongsTo('App\Project');
    }
}
