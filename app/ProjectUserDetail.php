<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProjectUserDetail extends Model
{
    protected $fillable = ['id', 'project_user_id', 'details',];
    
    public function user()
    {
        return $this->belongsTo('App\ProjectUser');
    }
    
}
