<?php

namespace App\Http\Controllers;

use App\Http\Requests\addProjectMemberRequest;
use App\Http\Requests\createPortofolioRequest;
use App\Http\Requests\updatePortofolioRequest;
use App\Http\Resources\ProjectResource;
use App\Project;
use Illuminate\Http\Request;
use Tymon\JWTAuth\Facades\JWTAuth;


class PortofolioController extends Controller
{
    public function getPortofolio()
    {
        $user = JWTAuth::parseToken()->authenticate();
        $projects = $user->projects()->paginate();
        return ProjectResource::collection($projects)->additional([
            'success' => true,
        ]);
    }

    public function getOwnedPortofolio()
    {
        $user = JWTAuth::parseToken()->authenticate();
        $projects = $user->myProjects()->paginate();
        return ProjectResource::collection($projects)->additional([
            'success' => true,
        ]);
    }

    public function getJoinedPortofolio()
    {
        $user = JWTAuth::parseToken()->authenticate();
        $projects = $user->joinedProjects()->paginate();
        return ProjectResource::collection($projects)->additional([
            'success' => true,
        ]);
    }

    public function getAllPortofolio()
    {
        $projects = Project::with('liking_users')->with('owner')->with('members')->paginate();
        return ProjectResource::collection($projects)->additional([
            'success' => true,
        ]);
    }

    public function getSinglePortofolio($id)
    {
        $user = JWTAuth::parseToken()->authenticate();
        $project = Project::with('liking_users')->with('owner')->with('members')->find($id);
        return $user->can('view', $project) ? ProjectResource::make($project) : response()->jsonError('no permission', 401);
    }

    public function createPortofolio(createPortofolioRequest $request)
    {
        $project = Project::create($request->toArray());
        $project->users()->attach(JWTAuth::parseToken()->authenticate(), ['permission' => 'owner', 'position' => 'owner']);
        return ProjectResource::make($project);
    }

    public function addPortofolioMember(addProjectMemberRequest $request)
    {
        $project = Project::find($request->project_id);
        $user = JWTAuth::parseToken()->authenticate();
        if ($user->can('view', $project)) {
            $project->users()->attach(JWTAuth::parseToken()->authenticate(), ['permission' => 'member', 'position' => $request->position]);
            return ProjectResource::make($project);
        }
        return response()->jsonError('no permission', 401);
    }

    public function removePortofolioMember(addProjectMemberRequest $request)
    {
        $project = Project::find($request->project_id);
        $user = JWTAuth::parseToken()->authenticate();
        if ($user->can('view', $project)) {
            $project->users()->detach(JWTAuth::parseToken()->authenticate(), ['permission' => 'member', 'position' => $request->position]);
            return ProjectResource::make($project);
        }
        return response()->jsonError('no permission', 401);
    }

    public function updatePortofolio(updatePortofolioRequest $request)
    {

    }

    public function deletePortofolio(int $id)
    {

    }

    public function addPortofolioToFavourite(Request $request)
    {
        $project = Project::find($request->id);
        $project->liking_users()->attach(JWTAuth::parseToken()->authenticate());
        return ProjectResource::make($project);
    }

    public function removePortofolioFromFavourite(Request $request)
    {
        $project = Project::find($request->id);
        $project->liking_users()->detach(JWTAuth::parseToken()->authenticate());
        return ProjectResource::make($project);
    }

}
