<?php

namespace App\Http\Controllers;

use App\Http\Requests\loginRequest;
use App\Http\Requests\registerRequest;
use App\Http\Resources\UserCollectionResource;
use App\Http\Resources\UserResource;
use App\Mail\VerifyRegistration;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Tymon\JWTAuth\Facades\JWTAuth;
use Illuminate\Support\Facades\Mail;

class UserController extends Controller
{
    public function doLogin(loginRequest $request)
    {

        $credentials = [
            'email' => $request->email,
            'password' => $request->password,
            'is_verified' => 1
        ];
        if (!Auth::attempt($credentials)) {
            return response()->jsonError('wrong credentials', 401);
        }
        return response()->jsonSuccess(JWTAuth::fromUser(Auth::user()));
    }

    public function getUser()
    {
        $user = JWTAuth::parseToken()->authenticate();
        return $user->can('view', $user) ? UserResource::make($user) : response()->jsonError('no permission', 401);
    }

    public function getAllUser()
    {
        return UserResource::collection(User::paginate())->additional([
            'success' => true,
        ]);
    }

    public function createUser(registerRequest $request)
    {
        $confirmation_code = str_random(15) . Hash::make($request->email . $request->phone_number) . str_random(12);
        $confirmation_code = str_replace('/', '', $confirmation_code);

        $user = User::create([
            'email' => $request->email,
            'password' => Hash::make($request->password),
            'verification_code' => $confirmation_code,
            'firstname' => ucwords(strtolower($request->firstname)),
            'lastname' => ucwords(strtolower($request->lastname)),
            'company' => $request->company,
            'organization' => $request->organization,
            'job_title' => $request->job_title,
            'gender' => $request->gender,
            'date_of_birth' => $request->date_of_birth,
            'phone_number' => $request->phone_number,
            'address' => $request->address,
            'privilege' => 'user'
        ])->searchable();

//        Mail::to($user->email)->send(new VerifyRegistration($user));

//        Mail::send('emails.verify', compact('confirmation_code'), function ($message) {
//            $message->to(Input::get('email'), Input::get('firstname') . " " . Input::get('lastname'))->subject('Verify your email address');
//        });

        return UserResource::make($user);
    }

    public function verify($confirmation_code)
    {
        if (!$confirmation_code) {
            return response()->jsonError('no confirmation code', 422);
        }
        $user = User::where('verification_code', $confirmation_code)->first();
        if (!$user) {
            return response()->jsonError('no confirmation for user found', 404);
        }
        $user->is_verified = true;
        $user->verification_code = null;
        $user->save();

        return UserResource::make($user);
    }

    public function testMail()
    {
        $user = User::find(3)->first();
        return new VerifyRegistration($user);
    }


    public function updateUser(int $id)
    {

    }
}
