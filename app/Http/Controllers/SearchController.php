<?php

namespace App\Http\Controllers;

use App\Project;
use App\User;
use Illuminate\Http\Request;

class SearchController extends Controller
{
    public function search(Request $request)
    {
        $projects = Project::search($request->q)->get();
        $users = User::search($request->q)->get();
        $results = [$projects,$users];
        dd($results);
        return array_flatten($results);
    }
}
