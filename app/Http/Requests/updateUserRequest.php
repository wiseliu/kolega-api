<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class updateUserRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return false;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'email' => 'email|unique:users',
            'password' => 'confirmed|min:8',
            'firstname' => 'regex:/^[(a-zA-Z\s)]+$/u',
            'lastname' => 'alpha',
            'date_of_birth' => 'date|before:today',
            'phone_number' => 'numeric|unique:users',
            'address' => 'string'
        ];
    }
}
