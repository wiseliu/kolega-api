<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class addProjectMemberRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return false;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'position' => 'string|required',
            'project_id' => 'numeric|required',
            'member_id' => 'numeric|required',
        ];
    }
}
