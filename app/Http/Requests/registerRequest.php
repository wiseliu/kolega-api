<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class registerRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'email' => 'required|email|unique:users',
            'password' => 'required|confirmed|min:8',
            'firstname' => 'required|regex:/^[(a-zA-Z\s)]+$/u',
            'lastname' => 'alpha',
            'gender' => 'required',
            'date_of_birth' => 'required|date|before:today',
            'phone_number' => 'required|numeric|unique:users',
            'address' => 'required|string'
        ];
    }
}
