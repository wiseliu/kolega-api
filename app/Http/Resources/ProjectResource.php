<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\Resource;

class ProjectResource extends Resource
{
    public $with = ['success' => true];
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request
     * @return array
     */
    public function toArray($request)
    {
        if ($this->owner != null) {
            return [
                'id' => $this->id,
                'name' => $this->firstname,
                'description' => $this->description,
                'status' => $this->status,
                'price' => $this->price,
                'owner' => !$this->owner->isEmpty() ? $this->owner[0] : null,
                'members' => $this->members,
                'likes_count' => $this->liking_users->count(),
                'likes_from' => $this->liking_users,
            ];
        }

    }

    /**
     * Get additional data that should be returned with the resource array.
     *
     * @param \Illuminate\Http\Request $request
     * @return array
     */
    public function with($request)
    {
        return [
            'success' => true,
        ];
    }
}
