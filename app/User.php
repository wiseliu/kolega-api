<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Laravel\Scout\Searchable;

class User extends Authenticatable
{
    use Notifiable;
    use Searchable;    

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function projects(){
	    return $this->belongsToMany('App\Project', 'project_user')->using('App\ProjectUser');
    }

    public function myProjects(){
        return $this->belongsToMany('App\Project', 'project_user')->using('App\ProjectUser')->wherePivot('permission', 'owner');
    }

    public function joinedProjects(){
        return $this->belongsToMany('App\Project', 'project_user')->using('App\ProjectUser')->wherePivot('permission', 'member');
    }

    public function projectFavourites(){
        return $this->hasMany('App\ProjectFavourite');
    }

    public function ProjectUsers(){
	    return $this->hasMany('App\ProjectUser');
    }

    public function liked_projects()
    {
        return $this->belongsToMany('App\Project','project_favourite')->using('App\ProjectFavourite')->withTimestamps();
    }
}
