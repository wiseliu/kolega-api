<?php

namespace App;

use Illuminate\Database\Eloquent\Relations\Pivot;

class ProjectUser extends Pivot
{
    protected $fillable = ['id', 'project_id', 'user_id', 'status', 'permission', 'position', 'approved_date',];
    
    public function user()
    {
        return $this->belongsTo('App\User');
    }
    
    public function project()
    {
        return $this->belongsTo('App\Project');
    }
    
    public function projectUserDetails()
    {
        return $this->hasMany('App\ProjectUserDetails');
    }
}
