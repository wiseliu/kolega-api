<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Laravel\Scout\Searchable;

class Project extends Model
{
    use Searchable;

    protected $fillable = ['id', 'name', 'description', 'status', 'tags', 'website_link', 'image_link', 'video_link', 'price',];

    public function users()
    {
        return $this->belongsToMany('App\User', 'project_user')->using('App\ProjectUser')->withPivot('status', 'approved_date')->withTimestamps();
    }

    public function owner()
    {
        return $this->belongsToMany('App\User', 'project_user')->wherePivot('permission', 'owner');
    }

    public function members()
    {
        return $this->belongsToMany('App\User', 'project_user')->using('App\ProjectUser')->wherePivot('permission', 'member');
    }

    public function ProjectUsers()
    {
        return $this->hasMany('App\ProjectUser')->with('user')->with('project');
    }

    public function projectFavourites()
    {
        return $this->hasMany('App\ProjectFavourite');
    }

    public function liking_users()
    {
        return $this->belongsToMany('App\User','project_favourite')->using('App\ProjectFavourite')->withTimestamps();
    }
}
