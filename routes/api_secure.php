<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::get('/getUser','UserController@getUser');
Route::get('/getAllUser','UserController@getAllUser');
Route::get('/getPortofolio','PortofolioController@getPortofolio');
Route::get('/getAllPortofolio','PortofolioController@getAllPortofolio');
Route::get('/getJoinedPortofolio','PortofolioController@getJoinedPortofolio');
Route::get('/getOwnedPortofolio','PortofolioController@getOwnedPortofolio');
Route::get('/getSinglePortofolio/{id}','PortofolioController@getSinglePortofolio');
Route::post('/createPortofolio','PortofolioController@createPortofolio');
Route::post('/addPortofolioToFavourite','PortofolioController@addPortofolioToFavourite');
Route::post('/removePortofolioFromFavourite','PortofolioController@removePortofolioFromFavourite');