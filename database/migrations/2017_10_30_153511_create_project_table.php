<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProjectTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('projects', function(Blueprint $table)
		{
			$table->increments('id');
			$table->string('name');
            $table->string('description');
            $table->string('status')->default('pending');
            $table->string('website_link')->nullable();
            $table->string('image_link')->nullable();
            $table->string('video_link')->nullable();
            $table->string('tags')->nullable();
            $table->double('price')->default(0.0);
            $table->nullableTimestamps();
		});
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('projects');        
    }
}
