<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToFeedCommentsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('feed_comments', function(Blueprint $table)
		{
			$table->foreign('feed_id')->references('id')->on('feeds')->onUpdate('RESTRICT')->onDelete('CASCADE');
			$table->foreign('posted_by')->references('id')->on('users')->onUpdate('RESTRICT')->onDelete('CASCADE');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('feed_comments', function(Blueprint $table)
		{
			$table->dropForeign('feed_comments_feed_id_foreign');
			$table->dropForeign('feed_comments_posted_by_foreign');
		});
	}

}
