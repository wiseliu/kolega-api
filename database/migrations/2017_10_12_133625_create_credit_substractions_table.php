<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateCreditSubstractionsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('credit_substractions', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('user_id')->unsigned()->default(0)->index('credit_substractions_user_id_foreign');
			$table->integer('credit_usage_package_id')->unsigned()->default(0)->index('credit_substractions_credit_usage_package_id_foreign');
			$table->integer('location_id')->unsigned()->nullable()->index('credit_substractions_location_id_foreign');
			$table->float('total_amount', 10, 0);
			$table->dateTime('valid_until')->nullable();
			$table->timestamps();
			$table->string('created_by');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('credit_substractions');
	}

}
