<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProjectUserDetailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('project_user_details', function (Blueprint $table) {
			$table->increments('id');
            $table->integer('project_user_id')->unsigned()->default(0);
            $table->foreign('project_user_id')->references('id')->on('project_user')->onDelete('cascade');
			$table->string('details');
            $table->nullableTimestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('project_user_details');
    }
}
