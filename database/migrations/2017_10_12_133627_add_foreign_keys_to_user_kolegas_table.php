<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToUserKolegasTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('user_kolegas', function(Blueprint $table)
		{
			$table->foreign('user_link1')->references('id')->on('users')->onUpdate('RESTRICT')->onDelete('CASCADE');
			$table->foreign('user_link2')->references('id')->on('users')->onUpdate('RESTRICT')->onDelete('CASCADE');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('user_kolegas', function(Blueprint $table)
		{
			$table->dropForeign('user_kolegas_user_link1_foreign');
			$table->dropForeign('user_kolegas_user_link2_foreign');
		});
	}

}
