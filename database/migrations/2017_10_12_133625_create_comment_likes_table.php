<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateCommentLikesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('comment_likes', function(Blueprint $table)
		{
			$table->integer('comment_id')->unsigned()->default(0)->index('comment_likes_comment_id_foreign');
			$table->integer('liked_by')->unsigned()->default(0)->index('comment_likes_liked_by_foreign');
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('comment_likes');
	}

}
