<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateProventsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('provents', function(Blueprint $table)
		{
			$table->increments('id');
			$table->string('name');
			$table->string('location');
			$table->text('description', 65535)->nullable();
			$table->string('img_url')->nullable();
			$table->dateTime('start_date');
			$table->dateTime('end_date');
			$table->boolean('approved');
			$table->enum('type', array('project','event'));
			$table->integer('user_id')->unsigned()->default(0)->index('provents_user_id_foreign');
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('provents');
	}

}
