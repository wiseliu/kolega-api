<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateUsersTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('users', function(Blueprint $table)
		{
			$table->increments('id');
			$table->string('email')->unique();
			$table->string('password');
			$table->string('firstname');
			$table->string('lastname');
			$table->string('company')->nullable();
			$table->string('organization')->nullable();
			$table->string('job_title')->nullable();
			$table->enum('gender', array('M','F'));
			$table->date('date_of_birth');
			$table->string('phone_number')->unique();
			$table->string('address');
			$table->string('photo_url')->nullable();
			$table->boolean('is_verified')->default(0);
			$table->string('verification_code')->nullable();
			$table->string('privilege');
			$table->rememberToken();
			$table->timestamps();
			$table->string('about')->nullable();
			$table->string('interest')->nullable();
			$table->string('web')->nullable();
			$table->string('ktp_number')->nullable()->unique();
			$table->dateTime('promo_code_created_date')->nullable();
			$table->string('promo_code')->nullable()->unique();
			$table->float('permanent_credit_amount', 10, 0)->default(0);
			$table->date('subscribed_until')->nullable();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('users');
	}

}
