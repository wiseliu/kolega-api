<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateFeedLikesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('feed_likes', function(Blueprint $table)
		{
			$table->integer('feed_id')->unsigned()->default(0)->index('feed_likes_feed_id_foreign');
			$table->integer('liked_by')->unsigned()->default(0)->index('feed_likes_liked_by_foreign');
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('feed_likes');
	}

}
