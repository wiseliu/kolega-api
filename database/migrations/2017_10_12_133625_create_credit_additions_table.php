<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateCreditAdditionsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('credit_additions', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('user_id')->unsigned()->default(0)->index('credit_additions_user_id_foreign');
			$table->integer('credit_purchase_package_id')->unsigned()->default(0)->index('credit_additions_credit_purchase_package_id_foreign');
			$table->integer('location_id')->unsigned()->default(0)->index('credit_additions_location_id_foreign');
			$table->float('amount', 10, 0);
			$table->date('valid_until')->nullable();
			$table->boolean('is_permanent');
			$table->timestamps();
			$table->string('created_by');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('credit_additions');
	}

}
