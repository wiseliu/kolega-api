<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToCreditAdditionsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('credit_additions', function(Blueprint $table)
		{
			$table->foreign('credit_purchase_package_id')->references('id')->on('credit_purchase_packages')->onUpdate('RESTRICT')->onDelete('CASCADE');
			$table->foreign('location_id')->references('id')->on('locations')->onUpdate('RESTRICT')->onDelete('CASCADE');
			$table->foreign('user_id')->references('id')->on('users')->onUpdate('RESTRICT')->onDelete('CASCADE');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('credit_additions', function(Blueprint $table)
		{
			$table->dropForeign('credit_additions_credit_purchase_package_id_foreign');
			$table->dropForeign('credit_additions_location_id_foreign');
			$table->dropForeign('credit_additions_user_id_foreign');
		});
	}

}
