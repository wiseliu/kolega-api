<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToCommentLikesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('comment_likes', function(Blueprint $table)
		{
			$table->foreign('comment_id')->references('id')->on('feed_comments')->onUpdate('RESTRICT')->onDelete('CASCADE');
			$table->foreign('liked_by')->references('id')->on('users')->onUpdate('RESTRICT')->onDelete('CASCADE');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('comment_likes', function(Blueprint $table)
		{
			$table->dropForeign('comment_likes_comment_id_foreign');
			$table->dropForeign('comment_likes_liked_by_foreign');
		});
	}

}
