<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateCreditSubstractionPaymentsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('credit_substraction_payments', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('credit_substraction_id')->unsigned()->default(0)->index('credit_substraction_payments_credit_substraction_id_foreign');
			$table->float('amount', 10, 0);
			$table->boolean('is_taken_from_permanent_credit');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('credit_substraction_payments');
	}

}
