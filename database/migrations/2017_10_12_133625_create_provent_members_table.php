<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateProventMembersTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('provent_members', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('provent_id')->unsigned()->default(0)->index('provent_members_provent_id_foreign');
			$table->integer('user_id')->unsigned()->default(0)->index('provent_members_user_id_foreign');
			$table->boolean('approved');
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('provent_members');
	}

}
