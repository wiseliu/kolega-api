<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToCreditSubstractionPaymentsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('credit_substraction_payments', function(Blueprint $table)
		{
			$table->foreign('credit_substraction_id')->references('id')->on('credit_substractions')->onUpdate('RESTRICT')->onDelete('CASCADE');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('credit_substraction_payments', function(Blueprint $table)
		{
			$table->dropForeign('credit_substraction_payments_credit_substraction_id_foreign');
		});
	}

}
