<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateFeedCommentsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('feed_comments', function(Blueprint $table)
		{
			$table->increments('id');
			$table->string('comment');
			$table->integer('feed_id')->unsigned()->default(0)->index('feed_comments_feed_id_foreign');
			$table->integer('posted_by')->unsigned()->default(0)->index('feed_comments_posted_by_foreign');
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('feed_comments');
	}

}
