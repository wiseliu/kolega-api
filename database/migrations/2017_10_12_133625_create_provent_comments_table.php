<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateProventCommentsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('provent_comments', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('provent_id')->unsigned()->default(0)->index('provent_comments_provent_id_foreign');
			$table->integer('user_id')->unsigned()->default(0)->index('provent_comments_user_id_foreign');
			$table->text('comment', 65535);
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('provent_comments');
	}

}
