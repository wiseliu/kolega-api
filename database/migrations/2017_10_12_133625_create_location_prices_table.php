<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateLocationPricesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('location_prices', function(Blueprint $table)
		{
			$table->increments('id');
			$table->string('name');
			$table->string('price_text');
			$table->string('icon_code');
			$table->string('icon_note')->nullable();
			$table->integer('location_id')->unsigned()->default(0)->index('location_prices_location_id_foreign');
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('location_prices');
	}

}
