<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToFeedLikesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('feed_likes', function(Blueprint $table)
		{
			$table->foreign('feed_id')->references('id')->on('feeds')->onUpdate('RESTRICT')->onDelete('CASCADE');
			$table->foreign('liked_by')->references('id')->on('users')->onUpdate('RESTRICT')->onDelete('CASCADE');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('feed_likes', function(Blueprint $table)
		{
			$table->dropForeign('feed_likes_feed_id_foreign');
			$table->dropForeign('feed_likes_liked_by_foreign');
		});
	}

}
