<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToAmenityLocationTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('amenity_location', function(Blueprint $table)
		{
			$table->foreign('amenity_id')->references('id')->on('amenities')->onUpdate('RESTRICT')->onDelete('CASCADE');
			$table->foreign('location_id')->references('id')->on('locations')->onUpdate('RESTRICT')->onDelete('CASCADE');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('amenity_location', function(Blueprint $table)
		{
			$table->dropForeign('amenity_location_amenity_id_foreign');
			$table->dropForeign('amenity_location_location_id_foreign');
		});
	}

}
