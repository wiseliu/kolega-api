<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToProventCommentsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('provent_comments', function(Blueprint $table)
		{
			$table->foreign('provent_id')->references('id')->on('provents')->onUpdate('RESTRICT')->onDelete('CASCADE');
			$table->foreign('user_id')->references('id')->on('users')->onUpdate('RESTRICT')->onDelete('CASCADE');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('provent_comments', function(Blueprint $table)
		{
			$table->dropForeign('provent_comments_provent_id_foreign');
			$table->dropForeign('provent_comments_user_id_foreign');
		});
	}

}
