<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateUserKolegasTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('user_kolegas', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('user_link1')->unsigned()->default(0)->index('user_kolegas_user_link1_foreign');
			$table->integer('user_link2')->unsigned()->default(0)->index('user_kolegas_user_link2_foreign');
			$table->boolean('confirmed')->default(0);
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('user_kolegas');
	}

}
